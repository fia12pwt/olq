/**
 * Created by JazzX on 25.11.2014.
 */
var ctx;
var connection;
exports.init= function (app, con) {
    ctx = app;
    connection = con;
};

exports.addUser = function(username,email,pwHash, status,res){
    var regDate = new Date;
    var data= {username: username,
        email : email,
        password : pwHash,
        status : status,
        reg_date : regDate};

        connection.query('insert into olq_users SET ?', data, function (fehler,result) {
            if (fehler)
            {
             //   throw new Error('Konnte User nicht hinzufügen');

              res.send({reg : 'fehler'});
            }
            else{
                res.send({reg:'ok'});
            }
        });
};
