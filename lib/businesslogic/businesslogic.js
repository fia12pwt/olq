/**
 * Created by JazzX on 25.11.2014.
 */
var ctx;
var db;
exports.register = require('./bl_register.js');
exports.login = require('./bl_login.js');
exports.init= function(app, db_layer,olq_config){
    ctx = app;
    db = db_layer;
    exports.login.init(ctx, olq_config);
    exports.register.init(ctx,db,olq_config);
};