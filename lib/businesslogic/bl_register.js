/**
 * Created by Flo on 25.11.2014.
 */

var ctx;
var db;
var pepper = 'pepper';
var crypto = require('crypto');
exports.init= function(app, db_layer,olq_config){
    ctx = app;
    db = db_layer;
    pepper = olq_config.password_pepper;
};

//Salt = pepper + UserID


exports.register = function(username, email, password,res){
        var pwhash = crypto.createHash('sha256');
        var salt = pepper + username;
        pwhash.update(password + salt);
        var h = pwhash.digest('base64');
        db.user.addUser(username, email, h, 0,res);
    };
