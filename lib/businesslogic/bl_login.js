/**
 * Created by Flo on 25.11.2014.
 */
var ctx;

var pepper = 'pepper';
var crypto = require('crypto');

// Initiert das Modul
exports.init= function(app, olq_config){
    ctx = app;
    pepper = olq_config.password_pepper;
};

/*//Salt = pepper + UserID
*/

//Generiert einen Passwordhash anhand des Passwortes, als salt wird das in der COnfig Hinterlegte pepper und der Benutzername genutzt
exports.hashPW= function(pw, username){
    var hash = crypto.createHash('sha256');
    var salt = pepper+username;
    hash.update(pw+salt);
    return hash.digest('base64');

};

