/**
 * Created by JazzX on 26.11.2014.
 */
var hashids = require('hashids');
var seed;

// Initiert das Modul mit dem link Salt aus der Config.JSON
exports.init =function(link_salt)
{
    seed = link_salt;
};

// Generiert den Kurzlinkhash anhand einer zufallszahl und
// mischt den ersten und letzten buchstaben des Usernamens mit ein
exports.genHash= function (username) {
    var generator = new hashids(seed);
    var first = username.charCodeAt(0);
    var last = username.charCodeAt(username.length-1);
    var rand = Math.floor((Math.random()*1000000)+1);
    var userseed =Math.floor((last+first)/2);
    var scode =generator.encode(userseed+rand);
    return scode;
}