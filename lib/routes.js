/*jslint node: true */
/*
Routen
*/

var ctx;
//APP Context

//Datalayer Einbinden und initialisieren
var database;
var olq_config;
//Businesslogik
var bl = require('./businesslogic/businesslogic.js');

exports.init = function (app, db_layer,olq_conf){
    ctx = app;
    database = db_layer;
    olq_config = olq_conf;
    bl.init (ctx, database,olq_conf);
};

//Homepage
exports.home = function (req, res) {
    res.render('home',{title: "URL kürzen", username: req.session.loggedin,pub_captcha: olq_config.captcha.public_key, admin: req.session.isAdmin});
};

//Impressum
exports.impressum = function (req, res) {
    console.log(ctx.olq_conf.impressum);
    res.render('impressum',{title: "Impressum", username: req.session.loggedin, impressum: ctx.olq_conf.impressum, admin: req.session.isAdmin});
};

//Datenschutz
exports.datenschutz = function (req, res) {
    res.render('datenschutz',{title: "Datenschutz", username: req.session.loggedin, admin: req.session.isAdmin});
};

//Nutzungsbedingungen
exports.nutzungsbedingungen = function (req, res) {
    res.render('nutzungsbedingungen',{title: "", username: req.session.loggedin, admin: req.session.isAdmin});
};

//About
exports.about = function (req, res) {
    res.render('about',{pageTestScript: '/qa/tests-about.js'});

};

//route
exports.route = function (req, res) {
    var slink = req.params.slink;
    var sel = 'select * from olq_slinks where slink='+ database.db_context.escape(slink);
    database.db_context.query(sel, function (err, result) {
        if (!err){
          if (result.length !== 0)
          {
            res.render('route',{title: "Weiterleitung", username: req.session.loggedin,slinkData : JSON.stringify(result), admin: req.session.isAdmin});
          }
          else
          {
            res.render('404');
          }
        }else
        {
            //res.render('route',{title: "Weiterleitung", username: req.session.loggedin,slinkData : 'noSLink', admin: req.session.isAdmin});
            res.render('500');
        }
    });
    // res.render('route',{title: "Weiterleitung", username: req.session.loggedin,slinkData : result});
};

//print
exports.print = function (req, res) {
    var slink = req.params.slink;
    var sel = 'select * from olq_slinks where slink='+ database.db_context.escape(slink);
    database.db_context.query(sel, function (err, result) {
        if (!err){
            console.log(result);
            if (result.length !== 0)
            {
              res.render('print',{title: "Print", username: req.session.loggedin,slinkData : JSON.stringify(result), admin: req.session.isAdmin});
            }
            else
            {
              res.render('404');
            }
        }else
        {
            console.log(err);
            //res.render('print',{title: "Print", username: req.session.loggedin,slinkData : 'noSLink', admin: req.session.isAdmin});
            res.render('500');
        }
    });
};

//profile
exports.profile = function (req, res) {
    if (req.session.loggedin) {
        var sel = 'select * from olq_users where username =' + database.db_context.escape(req.session.loggedin);
        database.db_context.query(sel, function (err, result) {
            if (!err) {
                console.log('Profildata: ' + result);
                var seladm = 'SELECT * FROM olq_conf WHERE conf_key ="admin" and conf_value = '+database.db_context.escape(result[0].users_id);
                database.db_context.query(seladm, function (err,resultadm) {
                    if(!err && resultadm.length > 0)
                    {
                        res.render('profile', {
                        title: "Profil",
                            username: req.session.loggedin,
                            status: result[0].status,
                            email: result[0].email,
                            regdate: result[0].reg_date,
                            admin: 'ok',
                            admin: req.session.isAdmin});
                    }
                    else
                    {
                        res.render('profile', {
                            title: "Profil",
                            username: req.session.loggedin,
                            status: result[0].status,
                            email: result[0].email,
                            regdate: result[0].reg_date
                        });
                    }
                });
            }
            else {

                res.render('500');

            }
        });
    }
    else
    {
        res.status(404);
        res.render('404');
    }
    //res.render('profile',{title: "Benutzerübersicht", username: req.session.loggedin, status: "Benutzer", email: "e@mail.de"});
};

//Register
exports.reg = function (req, res) {
    res.render('reg',{title: "Registrierung", username: req.session.loggedin,pub_captcha: olq_config.captcha.public_key, admin: req.session.isAdmin});
};

//Admin
exports.admin = function (req, res) {
    if(req.session.loggedin) {
        var sel = 'SELECT * from olq_users WHERE username = '+ database.db_context.escape(req.session.loggedin);
        database.db_context.query(sel, function (err,result) {
            if(!err)
            {
                var seladm = 'SELECT * FROM olq_conf WHERE conf_key ="admin" and conf_value = '+database.db_context.escape(result[0].users_id);
                database.db_context.query(seladm, function (err,resultadm) {
                    if(!err && resultadm.length >0)
                    {
                        res.render('admin', {title: "Admin", username: req.session.loggedin, admin: req.session.isAdmin});
                    }
                    else
                    {
                        res.render('500');
                    }
                })
            }
            else
            {
                res.render('500');
            }
        });

    }
    else
    {
        res.render('404');
    }
};

// 404 Page
exports.notFound = function (req, res) {
    res.status(404);
    res.render('404');
};

// 500 Page
exports.serverError = function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
};

// Middleware für Tests
exports.tests = function(req, res, next) {
    res.locals.showTests = ctx.get('env') !== 'production' &&
        req.query.test === '1';
    next();
};
