/*jslint node: true */
/*
 AJAX Handlers
 */

//APP Context
var ctx;

//Datalayer Einbinden und initialisieren
var database;
var re_captcha;
var olq_config;

//Businesslogik
var bl = require('./businesslogic/businesslogic.js');

var hashGen = require('./businesslogic/semacode.js');
var re_Captcha = require('no-captcha');

function checkInput(inputText) {
    var re = /<[\s\S]*>/;
    if(inputText == null)
    {
        return false;
    }
    else
    {
        var regresult = inputText.match(re);
        if (regresult != null) {
          return false;
        } else {
          return true;
        }
    }
  }
  // Initiert das Modul
exports.init = function(app, db_layer, olq_conf) {
  ctx = app;
  olq_config = olq_conf;
  database = db_layer;
  bl.init(ctx, database, olq_config);
  hashGen.init(olq_conf.link_salt);
  re_captcha = new re_Captcha(olq_config.captcha.public_key, olq_config.captcha.private_key);
};


// Löscht Session des angemeldeten Nutzers
exports.ajaxLogout = function(req, res) {
  req.session.destroy();
  res.send({
    logout: 'ok'
  });
};

// Meldet Nutzer an
// AjaxParam:   1. Username
// AjaxParam:   2. Password
exports.ajaxLogin = function(req, res) {
  //console.log('body: ' + JSON.stringify(req.body));
  var data = req.body;
  var username = data[0].value;
  var password = data[1].value;
  var pwhash = bl.login.hashPW(password, username);
  database.db_context.getConnection(function(err, connection) {
  var sel = 'select * from olq_users where username = ' + connection.escape(username);
  connection.query(sel, function(err, result) {
    if (!err && result.length > 0) {
      //console.log(result[0]);
      if (result[0].password === pwhash) {
        // Check ob Admin
        var seladm = 'SELECT * FROM olq_conf WHERE conf_key ="admin" and conf_value = ' + connection.escape(result[0].users_id);
        connection.query(seladm, function(err, resultadm) {
          if (!err && resultadm.length > 0) {
            req.session.loggedin = result[0].username;
            req.session.isAdmin = true;
            res.send({
              login: result[0].username,
              admin: 'ok'
            });
          } else {
            req.session.loggedin = result[0].username;
            req.session.isAdmin = false;
            res.send({
              login: result[0].username
            });
          }
        });
        // --------------

      } else {
        res.send({
          login: 'fehler'
        });
        //console.log('Login select fehler');
      }
    } else {
      res.send({
        login: 'fehler'
      });
    }
  });
  connection.release();
});
};


// Registriert Benutzer
// AjaxParam:   1. Benutzername
// AjaxParam:   2. Email
// AjaxParam:   3. Password
// AjaxParam:   4. Passwordcheck
exports.ajaxRegister = function(req, res) {
  var data = req.body;
  var captcha_data = {
    response: data[4].value,
    remoteip: req.connection.remoteAddress
  };
  console.log('ajax_Register body:' + JSON.stringify(data));
  re_captcha.verify(captcha_data, function(err, resp) {
    if (err === null) {
      if (!checkInput(data[0].value) || !checkInput(data[1].value)) {
        res.send({
          reg: 'fehler'
        });
      } else {

      console.log('captcha correct');
      if (data[2].value == data[3].value) {
        var reg = bl.register.register(data[0].value, data[1].value, data[2].value, res);
      } else {
        res.send({
          reg: 'fehler'
        });
      }
    }
    } else {
      console.log('incorrect captcha');
      res.send({
        reg: 'fehler'
      });
    };
  });
};

// Gibt Infos Des Angemeldeten Users als JSON zurrück
exports.ajaxGetUserdata = function(req, res) {
  var sel = 'select * from olq_users where username =' + database.db_context.escape(req.session.loggedin);
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      res.send(result[0]);
    } else {
      res.send({
        userdata: 'fehler'
      });
    }
  });
};

// Gibt eine JSON Liste mit allen dem Angemeldeten User gehörenden SLinks zurrück
// Keine Parameter
exports.ajaxGetUserSlinks = function(req, res) {
  var sel = 'select * from olq_users where username =' + database.db_context.escape(req.session.loggedin);
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      var sel_links = 'select * from olq_slinks where owner =' + result[0].users_id + ' ORDER BY create_date ASC';
      database.db_context.query(sel_links, function(err, links) {
        if (!err) {
          res.send(links);
        } else {
          res.send([{
            user_slinks: 'no_links'
          }]);
        }
      })
    } else {
      res.send({
        user_slinks: 'fehler'
      });
    }
  });
};

// Gibt Informationen zu dem angegebenen Kurzlinkhash zurrück
// AjaxParam:   1. SLink Hash
exports.ajaxGetSlinkData = function(req, res) {
  var sel = 'select * from olq_slinks where slink=' + database.db_context.escape(req.body[0].value);
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      res.send(result);
    } else {
      res.send({
        slink: 'notfound'
      });
    }
  });
};


// Fügt Kurzlink in die DB ein, wenn  kein angemeldeter User dann auf den User anon
// AjaxParam:   1. Orginal Url
//              2. Beschreibung
exports.ajaxInsertSlink = function(req, res) {
  var username;
  var data = req.body;
  var captcha_key;
  var lurl = data[0].value;
  if(lurl == "")
  {
    lurl = null;
  }
  var descr;
  if (req.session.loggedin) {
    captcha_key = data[2].value;
    descr = data[1].value;
  } else {
    captcha_key = data[1].value;
    descr = "";
  }

  var captcha_data = {
    response: captcha_key,
    remoteip: req.connection.remoteAddress
  };


  re_captcha.verify(captcha_data, function(err, resp) {
    if (err === null) {
      console.log("CAPTCHA CORRECT");
      //console.log(data);
      //var re =/<[\s\S]*>/;
      //var turl = lurl.match(re);
      //var tdescr = descr.match(re);
      //if (turl != null || tdescr != null){
      if (!checkInput(lurl) || !checkInput(descr)) {
          console.log("0");
        if (!checkInput(lurl) && !checkInput(descr)) {

            console.log("1");
            res.send({
                addslink: 'link + desc'
            });
        }
        else if(!checkInput(lurl) && checkInput(descr)) {
            console.log("2");
            res.send({
                addslink: 'link'
            });
        }
        else if(checkInput(lurl) && !checkInput(descr)) {
            console.log("3");
            res.send({
                addslink: 'desc'
            });
        }
      }
      else {

        if (req.session.loggedin) {
          username = req.session.loggedin;
        } else {
          username = 'anon';
        }
        var scode = hashGen.genHash(username);
        //console.log('Generierter ShortLink: '+scode);
        var sel = 'select * from olq_users where username =' + database.db_context.escape(username);
        var c_date = new Date();
        database.db_context.query(sel, function(err, result) {
          if (!err) {
            //console.log(result);
            var insert_data = {
              slink: scode,
              org_link: lurl,
              owner: result[0].users_id,
              create_date: c_date,
              description: descr,
              status: 0
            };
            var ins = 'insert into olq_slinks SET ?';
            database.db_context.query(ins, insert_data, function(err, result) {
              if (err) {
                res.send({
                  addslink: 'fehler'
                });
              } else {
                res.send(insert_data);
              }
            });
          } else {
            //console.log(err);
            res.send({
              addslink: 'fehler'
            });
          }
        });
      }
    }
    else
    {
        console.log("CAPTCHA WRONG");

        if (!checkInput(lurl) || !checkInput(descr)) {
          if (!checkInput(lurl) && !checkInput(descr)) {
              console.log("4");
              res.send({
                  addslink: 'link + desc + captcha'
              });
          }
          else if(!checkInput(lurl) && checkInput(descr)) {
              console.log("5");
              console.log(lurl);
              res.send({
                  addslink: 'link + captcha'
              });
          }
          else if(checkInput(lurl) && !checkInput(descr)) {
              console.log("6");
              res.send({
                  addslink: 'desc + captcha'
              });
          }
        }
        else
        {
            console.log("7");
            res.send({
            addslink: 'captcha'
            });
        }
    }
  });
};

//Liefert alle existierenden Nutzer zurück
// Keine eingabeparameter, Ausgabe: JSON Liste mit Usern
exports.ajaxGetAllUsers = function(req, res) {
  var sel = "SELECT username, email, date_format(reg_date, '%d.%m.%y %H:%i:%s') as reg_date FROM olq_users";
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      res.send(result);
    } else {
      res.send({
        allUsers: 'non'
      });
    }
  });
};


//Liefert alle angelegten Links zurück
// Keine eingabeparameter, Ausgabe: JSON Liste mit Links
exports.ajaxGetAllLinks = function(req, res) {
  var sel = "SELECT l.org_link, l.slink, u.username, date_format(l.create_date, '%d.%m.%y %H:%i:%s') as createdate FROM olq_slinks l, olq_users u WHERE l.owner = u.users_id ORDER BY create_date ASC;";
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      res.send(result);
    } else {
      res.send({
        allLinks: 'non'
      });
    }
  });
};

//Ändert Email des Angemeldeten Nutzers wenn Pw korrekt
// AjaxParam: 1. Password; 2. neue Email
exports.ajaxChangeEmail = function(req, res) {
  var data = req.body;
  console.log(data);
  var pwd = data.pw;
  var newemail = data.newMail;
  var sel = 'select * from olq_users where username =' + database.db_context.escape(req.session.loggedin);
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      var pwdhash = bl.login.hashPW(pwd, req.session.loggedin);
      if (pwdhash === result[0].password) {
        /*var upd_data = {
         users_id : result[0].users_id,
         email : newemail
         };
         var upd = 'UPDATE olq_users SET email = :email WHERE users_id = :users_id';*/
        var upd = 'UPDATE olq_users SET email = ' + database.db_context.escape(newemail) + ' WHERE users_id = ' + database.db_context.escape(result[0].users_id);
        database.db_context.query(upd, function(err, uresult) {
          if (!err) {
            res.send({
              email_change: 'ok'
            });
          } else {
            res.send({
              email_change: 'fehler'
            });
          }
        })
      } else {
        res.send({
          email_change: 'wrongPassword'
        });
      }
    } else {
      res.send({
        email_change: 'fehler'
      });
    }
  });
};


//Ändert Password des Angemeldeten Nutzers wenn altes Pw korrekt
// AjaxParam: 1. altes Password; 2. neues Password
exports.ajaxChangePassword = function(req, res) {
  var data = req.body;
  var pwd = data.pw;
  var newpwd = data.newpw;
  var sel = 'select * from olq_users where username =' + database.db_context.escape(req.session.loggedin);
  database.db_context.query(sel, function(err, result) {
    if (!err) {
      var pwdhash = bl.login.hashPW(pwd, req.session.loggedin);
      if (pwdhash === result[0].password) {
        /*var upd_data = {
         users_id : result[0].users_id,
         newpassword : newpwd
         };

         var upd = 'UPDATE olq_users SET password = :newpassword WHERE users_id = :users_id';*/
        var newPwHash = bl.login.hashPW(newpwd, req.session.loggedin);
        var upd = 'UPDATE olq_users SET password = ' + database.db_context.escape(newPwHash) + ' WHERE users_id = ' + database.db_context.escape(result[0].users_id);

        database.db_context.query(upd, function(err, uresult) {
          if (!err) {
            res.send({
              pw_change: 'ok'
            });
          } else {
            res.send({
              pw_change: 'fehler'
            });
          }
        })
      } else {
        res.send({
          pw_change: 'wrongPassword'
        });
      }
    } else {
      res.send({
        email_change: 'fehler'
      });
    }
  });
};

// Löscht SLink wenn der angemeldete User der Besitzer ist erwartet den SLINK hash
// AjaxParam: 1. slink
exports.ajaxDeleteUserSlink = function(req, res) {
  if (req.session.loggedin) {
    var username = req.session.loggedin;
    var slink = req.body.slink;
    var sel = 'select * from olq_users where username =' + database.db_context.escape(username);
    database.db_context.query(sel, function(err, result) {
      if (!err) {
        //TODO Check if User owns Slink
        var del = 'DELETE FROM olq_slinks where slink=' + database.db_context.escape(slink) + ' and owner = ' + database.db_context.escape(result[0].users_id);
        database.db_context.query(del, function(er, result) {
          if (!err) {
            res.send({
              deletelink: 'ok'
            });
          } else {
            res.send({
              deletelink: 'fehler'
            });
          }
        });
      } else {
        res.send({
          deletelink: 'fehler'
        });
      }
    });

  }
};

// Löscht SLink wenn der angemeldete User der Besitzer ist erwartet den SLINK hash
// AjaxParam: 1. slink
exports.ajaxDeleteSlink = function(req, res) {
  if (req.session.loggedin) {
    var username = req.session.loggedin;
    var slink = req.body.slink;
    var sel = 'SELECT * from olq_users WHERE username = ' + database.db_context.escape(req.session.loggedin);
    database.db_context.query(sel, function(err, result) {
      if (!err) {
        var seladm = 'SELECT * FROM olq_conf WHERE conf_key ="admin" and conf_value = ' + database.db_context.escape(result[0].users_id);
        database.db_context.query(sel, function(err, aresult) {
          if (!err && aresult.length > 0) {

            var del = 'DELETE FROM olq_slinks where slink=' + database.db_context.escape(slink);
            database.db_context.query(del, function(err, dresult) {
              if (!err) {
                res.send({
                  deletelink: 'ok'
                });
              } else {
                res.send({
                  deletelink: 'fehler'
                });
              }
            });
          } else {
            res.send({
              deletelink: 'fehler'
            });
          }
        });

      } else {
        res.send({
          deletelink: 'fehler'
        });
      }
    });
  }
};
