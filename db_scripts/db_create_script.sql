/*
Create Database
*/
CREATE DATABASE `olq` /*!40100 DEFAULT CHARACTER SET utf8 */;


/*
Create DB_USER
*/
CREATE USER 'olq_web'@'%' IDENTIFIED BY 'nuss';
GRANT ALL PRIVILEGES ON olq.* TO 'olq_web'@'%';  

USE OLQ;


/*
User Table
*******************************************
*/
CREATE TABLE `olq`.`olq_users` (
  `users_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL UNIQUE,
  `email` VARCHAR(45) NOT NULL UNIQUE,
  `password` VARCHAR(64) NOT NULL,
  `reg_date` TIMESTAMP NOT NULL,
  `status` INT NULL,
  PRIMARY KEY (`users_id`));
  
/*
Shortlinks
********************************************  
 */   
 CREATE TABLE `olq`.`olq_slinks` (
  `slink_id` INT NOT NULL AUTO_INCREMENT,
  `slink` VARCHAR(256) NOT NULL,
  `org_link` VARCHAR(2048) NOT NULL,
  `owner` INT NULL,
  `create_date` TIMESTAMP NOT NULL,
  `description` VARCHAR(45) NULL,
  `status` INT NULL,
  PRIMARY KEY (`slink_id`),
  INDEX `owner_idx` (`owner` ASC),
  CONSTRAINT `owner`
    FOREIGN KEY (`owner`)
    REFERENCES `olq`.`olq_users` (`users_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
	
/*
Configuration
********************************************
*/	
	
CREATE TABLE `olq`.`olq_conf` (
  `conf_id` INT NOT NULL AUTO_INCREMENT,
  `conf_key` VARCHAR(45) NOT NULL,
  `conf_value` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`conf_id`));

/*
Anon User erstellen
********************************************
*/

INSERT INTO `olq`.`olq_users` (`username`, `email`, `password`) VALUES ('anon', 'nop@nop.de', 'locked');
