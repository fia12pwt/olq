{"db_config":{
  "host" : "",
  "user" :"",
  "password":"",
  "database":""
  },
  "password_pepper":"pepper",
  "link_salt":"olq",
  "port":61000,

  "impressum": {
    "name": "Betreibername",

    "address": {
      "city": "Musterstadt",
      "country": "Deutschland",
      "street": "Musterstraße 1"
    },

    "email": "admin@email.de",
    "phone": "01234/14556"
  },
  "captcha":{
    "public_key":"",
    "private_key":""
  }
}
