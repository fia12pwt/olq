/*jslint node: true */

var express = require('express');
var app = express();
var session = require('express-session');
var olq_config = require('./config.json');
var re_captcha = require('no-captcha');
app.olq_conf = olq_config;
var handlebars = require('express-handlebars').create({
    defaultLayout: 'main',
    helpes: {
        section: function (name, options) {
            if (!this._sections) this._sections = {};
            this._sections[name] = options.fn(this);
            return null;
        }
    }
});

var bodyParser = require('body-parser');

//Datalayer Einbinden und initialisieren
var database = require('./lib/database.js');
database.init(app,olq_config.db_config);

//Routen einbinden und initialisieren
var routes = require('./lib/routes.js');
routes.init(app,database,olq_config);

//AJAX Handler einbinden und initialisieren
var ajax = require('./lib/ajax.js');
ajax.init(app, database,olq_config);

// Template Engine
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

//Sessionmanagement
app.set('trust proxy', 1);
app.use(session({secret: 'keyboard cat',
                resave: true,
                saveUninitialized: true}));

// Port
app.set('port', process.env.PORT || olq_config.port);

//Static Content
app.use(express.static(__dirname + '/public'));

//****** AJAX Handlers ********************
app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

app.post('/main/dynamic/action/login', ajax.ajaxLogin);
app.post('/main/dynamic/action/logout', ajax.ajaxLogout);
app.post('/main/dynamic/action/register', ajax.ajaxRegister);
app.post('/main/dynamic/action/userdata', ajax.ajaxGetUserdata);
app.post('/main/dynamic/action/userslinks', ajax.ajaxGetUserSlinks);
app.post('/main/dynamic/action/insertslink', ajax.ajaxInsertSlink);
app.post('/main/dynamic/action/ajaxDeleteUserSlink', ajax.ajaxDeleteUserSlink);
app.post('/main/dynamic/action/ajaxDeleteSlink', ajax.ajaxDeleteSlink);
app.post('/main/dynamic/action/getAllLinks', ajax.ajaxGetAllLinks);
app.post('/main/dynamic/action/getAllUsers', ajax.ajaxGetAllUsers);

app.post('/main/dynamic/action/ajaxChangeEmail', ajax.ajaxChangeEmail);
app.post('/main/dynamic/action/ajaxChangePassword', ajax.ajaxChangePassword);
//****** Routes zu den Seiten *******************

//Homepage
app.get('/', routes.home);

//About
app.get('/about', routes.about);

//print
app.get('/p/:slink', routes.print);

//Register
app.get('/reg', routes.reg);

//profil
app.get('/profile', routes.profile);

//admin
app.get('/admin', routes.admin);

// Impressum
app.get('/impressum', routes.impressum);

// Datenschutz
app.get('/datenschutz', routes.datenschutz);

// Nutzungsbedingungen
app.get('/nutzungsbedingungen', routes.nutzungsbedingungen);

//Weiterleitung
app.get('/:slink', routes.route);

// 404 Page
app.use(routes.notFound);

// 500 Page
app.use(routes.serverError);

//Middleware Tests
app.use(routes.tests);

// Server Start
app.listen(app.get('port'), function () {

    console.log('Server gestartet ( http://localhost:' + app.get('port') + '; STRG + C zum Beenden');
    //Testuser in die Datenbank schreiben
    //console.log(config);

});
