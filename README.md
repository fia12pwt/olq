# README #

##Description
Linkshortener with QR-Code Generator

![olq_captcha.png](https://bitbucket.org/repo/Ln6Exy/images/629074673-olq_captcha.png)

##Install

 1. Install dependecies with "npm install"
 2. Setup the MySQL database with the script in the db_scripts folder ( change the user password ! )
 3. Customize the configuration in the config.json to your needs
 4. To run OLQ as a service, install *forever* with "sudo npm install forever -g"
 5. Start OLQ with one of the following commands:
      - as a service: "forever start -s app.js"
      - standalone with npm: "npm start"
      - standalone with node: "node app.js"
 6. To access OLQ, open "localhost:[PORT]" in your browser (the port is set in the config.json)
 7. To use the version with captcha implementation you need to to clone the no_captcha branch, register a key pair at [google recaptcha](http://www.google.com/recaptcha/intro/index.html) and insert them in the config.json

For security reasons it's recommended to use an apache2 or nginx as a reversed proxy to enable *https* and to bind the service to the correct (sub-)domain and external port.

##Authors:
 - Daniel Depta
 - Florian Vollmann
 - Marko Schulze
 - Robert Werth

##Licence:

BSD
